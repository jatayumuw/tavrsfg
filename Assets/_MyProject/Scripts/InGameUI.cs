using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;

public class InGameUI : MonoBehaviour
{
    public InputActionProperty leftSecondary, rightSecondary;
    public GameObject pauseUI, gameUI, finalUI;

    float timer = 0;
    public bool isRunning;
    public TextMeshProUGUI timerTextGame, timerTextPause, timerTextFinal;

    UIButton UIButtonHandler;
    GameObject ManagerScene;

    void Awake()
    {
        ManagerScene = GameObject.Find("Manager");
    }

    private void Start()
    {
        gameUI.SetActive(true);
        pauseUI.SetActive(false);

        isRunning = true;
    }

    void Update()
    {
        //OnPressedBY();

        TimeCounter();
        isRunning = true;
    }

    void TimeCounter()
    {
        if (isRunning) timer += Time.deltaTime;

        int minutes = Mathf.FloorToInt(timer / 60F);
        int seconds = Mathf.FloorToInt(timer - minutes * 60);
        timerTextGame.text = string.Format("{00:00}:{01:00}", minutes, seconds);
        timerTextPause.text = string.Format("{00:00}:{01:00}", minutes, seconds);
        timerTextFinal.text = string.Format("{00:00}:{01:00}", minutes, seconds);
    }

    void OnPressedBY()
    {
        //pause game
        if (leftSecondary.action.IsPressed() && rightSecondary.action.IsPressed() && isRunning)
        {
            isRunning = false;

            gameUI.SetActive(false);
            pauseUI.SetActive(true);
        }
        //resume 
        else if (leftSecondary.action.IsPressed() && rightSecondary.action.IsPressed() && !isRunning)
        {
            isRunning = true;

            gameUI.SetActive(true);
            pauseUI.SetActive(false);
        }
    }

    //Ini Button Lohh
    public void Resume()
    {
        isRunning = true;

        gameUI.SetActive(true);
        pauseUI.SetActive(false);
    }
    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
        ManagerScene.GetComponent<UIButton>().scene = 0;
    }
    void Restart()
    {
        if (ManagerScene.GetComponent<UIButton>().scene == 1) SceneManager.LoadScene("Tutorial");
        if (ManagerScene.GetComponent<UIButton>().scene == 2) SceneManager.LoadScene("Simulation");
        if (ManagerScene.GetComponent<UIButton>().scene == 3) SceneManager.LoadScene("Test");
    }

    public void Final()
    {
        isRunning = false;
        finalUI.SetActive(true);
    }
}

