using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PapanTulisTutorial : MonoBehaviour
{
    public GameObject image1, image2, image3;
    public TextMeshProUGUI keterangan;
    [Range(1, 3)] public int display;

    void Start()
    {
        image1.SetActive(false);
        image2.SetActive(false);
        image3.SetActive(false);
    }
    IEnumerator TimerCoroutine()
    {
        if (display == 1)
        {
            display++;
            image1.SetActive(true);
            image2.SetActive(false);
            image3.SetActive(false);
        }
        else if (display == 2)
        {
            display++;
            image1.SetActive(false);
            image2.SetActive(true);
            image3.SetActive(false);
        }
        else if (display == 3)
        {
            display = 1;
            image1.SetActive(false);
            image2.SetActive(false);
            image3.SetActive(true);
        }
        yield return new WaitForSeconds(5);
    }

}
