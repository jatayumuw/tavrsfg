using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.InputSystem;
using TMPro;

public class InputController : MonoBehaviour
{
    public Wave waveHandler;

    [Header("Value For Wave")]
    [Range(1, 5)] public int waveTypes; //Jenis Glombang
    [Range(0, 100)] public int amplitude; //AMPL
    [Range(1, 125)] public int waveSpeed; //Cepat Glombang
    [Range(1, 5)] public int sweepFrequency; //Sweep
    [Range(1, 100)] public int voltsPerDiv; // 1/AMPL
    [Range(1, 100)] public int timePerDiv ; //Freq
    
    [Header("Rotator Number Attribute")]
    [Range(1, 4)] public int control;
    public GameObject Rotator, RightHandTransform;
    public int RotatorInit, RotatorUpdate;
    float rValue, hValue;
    public TextMeshProUGUI uwu;

    [Header("Button Number Attribute")]
    public int valueDigit;
    public List<string> valuePerDigit;
    int valueDigitIndex;

    void Update()
    {
        rValue = Rotator.transform.eulerAngles.y;
        hValue = RightHandTransform.transform.eulerAngles.z;

        //uwu.text = hValue.ToString();
        WaveAmplitudeChanger();

        waveHandler.WaveTypes = waveTypes;
        waveHandler.waveSpeed = waveSpeed * 0.05f;
        waveHandler.timePerDiv  = timePerDiv  * 0.01f;
        waveHandler.voltsPerDiv = (voltsPerDiv * 0.01f) + 0.39f;
    }
//////Amplitude changer based on wavetypes
    public void WaveAmplitudeChanger()
    {
        if (waveHandler.WaveTypes == 1 || waveHandler.WaveTypes == 3) waveHandler.amplitude = amplitude * (0.3276f/100);
        if (waveHandler.WaveTypes == 2 || waveHandler.WaveTypes == 4 || waveHandler.WaveTypes == 5) waveHandler.amplitude = amplitude * (0.2048f/100);
    }

//////Rotator related things
    public void ValueRotator()
    {
        if (control == 1)
            waveTypes = (((RotatorUpdate - RotatorInit) * 2) / (360 / 5)) + 1;
        else if (control == 2)
            timePerDiv  = (((RotatorUpdate - RotatorInit) * 2) / (360 / 100)) + 1;
        else if (control == 3)
            amplitude = (((RotatorUpdate - RotatorInit) * 2) / (360 / 100)) + 1;
        else if (control == 4)
            waveSpeed = (((RotatorUpdate - RotatorInit) * 2) / (360 / 125)) + 1;

        Debug.Log("waveTypes: " + waveTypes);
        Debug.Log("timePerDiv: " + timePerDiv);
        Debug.Log("amplitude: " + amplitude);
        Debug.Log("waveSpeed: " + waveSpeed);
    }
    public void RotatorNext()
    {
        Debug.Log(control + " value is min +1");
        if (control == 1 && waveTypes < 5) waveTypes = waveTypes + 1;
        else if (control ==  2 && timePerDiv  < 100) timePerDiv  = timePerDiv  + 1;
        else if (control == 3 && amplitude < 100) amplitude = amplitude + 1;
        else if (control == 4 && waveSpeed < 125) waveSpeed = waveSpeed + 1;
    }//UICanvas
    public void RotatorPrev()
    {
        Debug.Log(control + " value is min -1");
        if (control == 1 && waveTypes > 1) waveTypes = waveTypes - 1;
        else if (control == 2 && timePerDiv  > 1) timePerDiv  = timePerDiv  - 1;
        else if (control == 3 && amplitude > 0) amplitude = amplitude - 1;
        else if (control == 4 && waveSpeed > 1) waveSpeed = waveSpeed - 1;
    }//UICanvas

//////Numpad related things
    public void SetupDigit(int number)
    {
        SetupDigitSetting();
        string tempDigit = "";

        if (valuePerDigit.Count > 1) 
        {
            if (valueDigitIndex == 2 && valuePerDigit[valueDigitIndex] != "")
            {
                valuePerDigit[0] = valuePerDigit[1];
                valuePerDigit[1] = valuePerDigit[2];
            }

            valuePerDigit[valueDigitIndex] = number.ToString();

            if (valueDigitIndex < 2)
                valueDigitIndex++;

            for (int i = 0; i < valuePerDigit.Count; i++)
                if (valuePerDigit[i] != "")
                    tempDigit += valuePerDigit[i];
        }
        else
            valuePerDigit[0] = tempDigit = number.ToString();

        valueDigit = int.Parse(tempDigit);
    }//UICanvas

    public void SetupDigitSetting()
    {
        if (control == 1)
        {
            waveTypes = valueDigit;
            if(valueDigit < 1) waveTypes = 1; if(valueDigit > 5) waveTypes = 5;
        }
        else if (control == 2)
        {
            timePerDiv  = valueDigit;
            if(valueDigit < 1) timePerDiv  = 1; if(valueDigit > 100) timePerDiv  = 100;
        }
        else if (control == 3)
        {
            amplitude = valueDigit;
            if(valueDigit < 1) amplitude = 1; if(valueDigit > 100) amplitude = 100;
        }
        else if (control == 4)
        {
            waveSpeed = valueDigit;
            if(valueDigit < 1) waveSpeed = 1; if(valueDigit > 125) waveSpeed = 125;
        }
    }//UICanvas
    public void Dedicated(int index)
    {
        control = index;
        RotatorInit = RotatorUpdate;
        Debug.Log("Current control number: " + waveTypes);

        if (control == 1)
        {
            valuePerDigit.Clear();
            valuePerDigit.Add("");
        }
        else if (control == 2 || control == 3 || control == 4)
        {
            valuePerDigit.Clear();
            valuePerDigit.Add("");
            valuePerDigit.Add("");
            valuePerDigit.Add("");
        }
    }//UICanvas
    public void Function(int index)
    {
        waveTypes = index;
        Debug.Log("Function button no: " + waveTypes +  " is clicked");
    }//UICanvas

    public void OnMouseDown()
    {
        rValue = hValue;
        RotatorUpdate = ((int)rValue + 90) * 2;
        if (RotatorUpdate > 360) RotatorUpdate = 360;
        else if (RotatorUpdate < 0) RotatorUpdate = 0;

        Debug.Log("value: " + RotatorUpdate);
        Debug.Log("rValue: " + rValue + " && " + "hValue: " + hValue);
    }
}
