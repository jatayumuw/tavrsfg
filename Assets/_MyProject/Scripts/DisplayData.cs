using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayData : MonoBehaviour
{
    public List<GameObject> blinkingText;
    public InputController InputControllerHandler;
    private bool isKedip;

    // Update is called once per frame
    void Update()
    {
        blinkingText[3].GetComponent<TextMeshProUGUI>().text = InputControllerHandler.waveSpeed.ToString();
        blinkingText[2].GetComponent<TextMeshProUGUI>().text = InputControllerHandler.amplitude.ToString();
        blinkingText[1].GetComponent<TextMeshProUGUI>().text = InputControllerHandler.timePerDiv.ToString();
        TextWaveform();

        if (!isKedip)
        {
            isKedip = true;
            StartCoroutine(BlinkingTextController(InputControllerHandler.control));
        }
    }

    public void TextWaveform()
    {
        if (InputControllerHandler.waveTypes == 1) blinkingText[0].GetComponent<TextMeshProUGUI>().text = "Sine";
        else if (InputControllerHandler.waveTypes == 2) blinkingText[0].GetComponent<TextMeshProUGUI>().text = "Triangle";
        else if (InputControllerHandler.waveTypes == 3) blinkingText[0].GetComponent<TextMeshProUGUI>().text = "Square";
        else if (InputControllerHandler.waveTypes == 4) blinkingText[0].GetComponent<TextMeshProUGUI>().text = "Ramp";
        else if (InputControllerHandler.waveTypes == 5) blinkingText[0].GetComponent<TextMeshProUGUI>().text = "Negative Ramp";

        else if (InputControllerHandler.waveTypes >5 || InputControllerHandler.waveTypes < 1)
            blinkingText[0].GetComponent<TextMeshProUGUI>().text = "Error :v";
    }

    public IEnumerator BlinkingTextController(int input)
    {
        for (int i = 0; i < blinkingText.Count; i++)
        {
            if (i + 1 == input)
            {
                if (blinkingText[i].activeInHierarchy) blinkingText[i].SetActive(false);
                else blinkingText[i].SetActive(true);
            }
            else
            {
                blinkingText[i].SetActive(true);
            }
        }
        yield return new WaitForSeconds(0.4f);
        isKedip = false;
    }
}
