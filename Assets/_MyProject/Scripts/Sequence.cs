using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sequence : MonoBehaviour
{
    public GameObject pintuLemari1, pintuLemari2;
    public GameObject powerSwitch, powerButton;
    public GameObject Line, Blackout;
    public GameObject UIExit;

    public bool startAFG, BSC, FBC, pintu1, pintu2;

    [Range(0,2)] public int sequenceTotal;
    [Range(0, 3)] public int sequencePower, sequencePengambilan;
    public float buttonShiftValue, pintuShiftValue;

    private void Start()
    {
        StartCoroutine(SequenceMove());   
    }


    private void Update()
    {
        UIExitButton();
        SetSequenceTotal();
    }

    public void UIExitButton()
    {
        if (startAFG) UIExit.SetActive(true);
        if (!startAFG) UIExit.SetActive(false);
    }
    public void SetSequenceTotal()
    {
        //block
        if (sequenceTotal < 2)
        {
            Line.SetActive(false);
            Blackout.SetActive(true);
        }
        else if (sequenceTotal == 2)
        {
            startAFG = true;
            Line.SetActive(true);
            Blackout.SetActive(false);
        }
    }

    IEnumerator SequenceMove()
    {
        if(sequenceTotal > -1 && sequenceTotal <3)
        {
            yield return new WaitUntil(() => sequencePengambilan == 3);
            sequencePower++;
            sequenceTotal++;

            yield return new WaitUntil(() => sequencePower == 3);
            sequenceTotal++;
        }
        yield return null;
    }

    //nambah value sequence
    public void PowerSequence(int i)
    {
        if (i == 1) sequencePower++;
        if (i == 2) sequencePower--;
    }
    public void AmbilSequence(int i)
    {
        if (i == 1) sequencePengambilan++;
        if (i == 2) sequencePengambilan--;
    }

    //buat button power depan blakang
    public void BackSwitchClick()
    {
        if (!BSC)
        {
            powerSwitch.transform.Rotate(0, 0, 8, Space.Self);
            sequencePower++;
            BSC = true;
        }
        else if (BSC)
        {
            powerSwitch.transform.Rotate(0, 0, -8, Space.Self);
            sequencePower--;
            BSC = false;
        }
    }
    public void FrontButtonClick()
    {

        if (!FBC)
        {
            powerButton.transform.localPosition = powerButton.transform.localPosition + new Vector3(0, buttonShiftValue, 0);
            sequencePower++;
            FBC = true;
        }
        else if (FBC)
        {
            powerButton.transform.localPosition = powerButton.transform.localPosition + new Vector3(0, -buttonShiftValue, 0);
            sequencePower--;
            FBC = false;
        }
    }

    //buat button pintu lemari (lemari1 == kiri; lemari2 == kanan)
    public void OpenSesame(int i)
    {
        if (i== 1 && !pintu1)
        {
            StartCoroutine(lemari1Coroutine());
        } 
        
        else if (i==2 && !pintu2)
        {
            StartCoroutine(lemari2Coroutine());
        }
    }

    IEnumerator lemari1Coroutine()
    {
        pintu1 = true;
        pintuLemari1.transform.localPosition = pintuLemari1.transform.localPosition + new Vector3(pintuShiftValue, 0, 0);

        yield return new WaitForSeconds(8);

        pintuLemari1.transform.localPosition = pintuLemari1.transform.localPosition + new Vector3(-pintuShiftValue, 0, 0);
        pintu1 = false;
    }
    IEnumerator lemari2Coroutine()
    {
        pintu1 = true;
        pintuLemari2.transform.localPosition = pintuLemari2.transform.localPosition + new Vector3(pintuShiftValue, 0, 0);

        yield return new WaitForSeconds(8);

        pintuLemari2.transform.localPosition = pintuLemari2.transform.localPosition + new Vector3(-pintuShiftValue, 0, 0);
        pintu1 = false;
    }
}
