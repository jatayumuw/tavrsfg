using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{

    public Camera FirstPersonCamera;

    public Vector3 offsetPos;
    public Vector3 pos;

    [Range(0, 1)]
    public float smoothFactor;

    private void Update()
    {
        Vector3 currentPos = transform.position;
        Vector3 cameraCenter = FirstPersonCamera.transform.position + FirstPersonCamera.transform.forward;
        Vector3 direction = currentPos - cameraCenter;
        Vector3 targetPosition = FirstPersonCamera.transform.TransformPoint(offsetPos);

        transform.position = Vector3.Lerp(currentPos, targetPosition, smoothFactor);
        transform.rotation = Quaternion.LookRotation(direction);
    }
}
