﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SineWave : MonoBehaviour
{
    private LineRenderer lineRenderer;

    const int numberVerticalDivs = 8;
    const int numberHorizontalDivs = 10;

    public float sweepFrequency = 40;
    public float voltsPerDiv = .5f;
    public float timePerDiv;

    [SerializeField]
    [Range(1, 10)]
    public int wavelength;

    [SerializeField]
    [Range(0, 8)]
    public float amplitude;

    public float frequency;

    public Vector3 tempat;
    private Transform tempats;

    [SerializeField]
    [Range(0, 10)]
    public float waveSpeed;

    public float SecondsPerDiv
    {
        get => 1f / (numberHorizontalDivs * sweepFrequency);

        set
        {
            sweepFrequency = 1f / (numberHorizontalDivs * value);
        }
    }

    public float SweepFrequency
    {
        get => sweepFrequency;

        set
        {
            sweepFrequency = value;
        }
    }

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        Vector3 positions = transform.TransformPoint(tempat);
        DrawSineWave(positions, amplitude, wavelength, waveSpeed, frequency, timePerDiv);
        
        
    }


    public void DrawSineWave(Vector3 positions, float amplitude, int wavelength, float waveSpeed, float frequency, float timePerDiv)
    {
        float x = 0f;
        float y;
        float a = amplitude / voltsPerDiv;
        //float a = amplitude / 2;
        //rumus banyak gelombang 2PI/Lambda
        // k = banyak gelombang 
        // w = frekuensi sudut
        float k = Mathf.PI * frequency * (timePerDiv*10);
        float w = k * waveSpeed;
        frequency = wavelength * waveSpeed;
        timePerDiv = frequency;
        float p = 2 * Mathf.PI / k;
        lineRenderer.positionCount = 65;
        for (int i = 0; i < lineRenderer.positionCount; i++)
        {   
            x += i * 0.001f;
            //Gelombang Sinus dengan rumus A sin(kx - wt) 
            y = a * Mathf.Sin(k * x - w * Time.time);
            lineRenderer.SetPosition(i, new Vector3(x, y, 0) + tempat);
        }
    }
}
