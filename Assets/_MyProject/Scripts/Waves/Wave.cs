﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    private LineRenderer lineRenderer;

    const int numberVerticalDivs = 8;
    const int numberHorizontalDivs = 10;
    public int lineSize = 65;

    public float sweepFrequency = 40;
    public float voltsPerDiv;
    public float timePerDiv;

    public int WaveTypes;
    public float wavelength;
    public float amplitude;
    public float frequency;

    public Vector3 tempat;
    private Transform tempats;

    [SerializeField]
    [Range(0, 5)]
    public float waveSpeed;

    public float SecondsPerDiv
    {
        get => 1f / (numberHorizontalDivs * sweepFrequency);

        set
        {
            sweepFrequency = 1f / (numberHorizontalDivs * value);
        }
    }

    public float SweepFrequency
    {
        get => sweepFrequency;

        set
        {
            sweepFrequency = value;
        }
    }

    void Start()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    private void Update()
    {
        Vector3 positions = transform.TransformPoint(tempat);

        if (WaveTypes == 1)
            DrawSine(positions, amplitude, wavelength, waveSpeed, frequency, timePerDiv);
        if (WaveTypes == 2)
            DrawTriangle(positions, amplitude, wavelength, waveSpeed, frequency, timePerDiv);
        if (WaveTypes == 3)
            DrawSquare(positions, amplitude, wavelength, waveSpeed, frequency, timePerDiv);
        if (WaveTypes == 4)
            DrawRamp(positions, amplitude, wavelength, waveSpeed, frequency, timePerDiv);
        if (WaveTypes == 5)
            DrawNRamp(positions, amplitude, wavelength, waveSpeed, frequency, timePerDiv);
    }

    //float a = amplitude / 2;
    //rumus banyak gelombang 2PI/Lambda
    // k = banyak gelombang 
    // w = frekuensi sudut
    public void DrawSine(Vector3 positions, float amplitude, float wavelength, float waveSpeed, float frequency, float timePerDiv)
    {
        float x = 0f;
        float y;
        float a = amplitude / voltsPerDiv;
        float k = Mathf.PI * frequency * (timePerDiv * 10);
        float w = k * waveSpeed;
        frequency = wavelength * waveSpeed;
        timePerDiv = frequency;
        float p = 2 * Mathf.PI / k;
        lineRenderer.positionCount = lineSize;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            x += i * 0.001f;
            y = a * Mathf.Sin(k * x - w * Time.time);
            lineRenderer.SetPosition(i, new Vector3(x, y, 0) + tempat);
        }
    }
    public void DrawTriangle(Vector3 positions, float amplitude, float wavelength, float waveSpeed, float frequency, float timePerDiv)
    {
        float x = 0f;
        float y;
        float a = amplitude / voltsPerDiv;
        float k = Mathf.PI * frequency * (timePerDiv * 10);
        float w = k * waveSpeed;
        frequency = wavelength * waveSpeed;
        timePerDiv = frequency;
        float p = 2 * Mathf.PI / k;
        lineRenderer.positionCount = lineSize;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            x += i * 0.001f;
            y = a * Mathf.Asin(Mathf.Sin(k * x - w * Time.time));
            lineRenderer.SetPosition(i, new Vector3(x, y, 0) + tempat);
        }
    }
    public void DrawSquare(Vector3 positions, float amplitude, float wavelength, float waveSpeed, float frequency, float timePerDiv)
    {
        float x = 0f;
        float y;
        float a = amplitude / voltsPerDiv;
        float k = Mathf.PI * frequency * (timePerDiv * 10);
        float w = k * waveSpeed;
        frequency = wavelength * waveSpeed;
        timePerDiv = frequency;
        float p = 2 * Mathf.PI / k;
        lineRenderer.positionCount = lineSize;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            x += i * 0.001f;
            y = a * Mathf.Sign(Mathf.Sin(k * x - w * Time.time));
            lineRenderer.SetPosition(i, new Vector3(x, y, 0) + tempat);
        }
    }
    public void DrawRamp(Vector3 positions, float amplitude, float wavelength, float waveSpeed, float frequency, float timePerDiv)
    {
        float x = 0f;
        float y;
        float a = amplitude / voltsPerDiv;
        float k = Mathf.PI * frequency * (timePerDiv * 10);
        float w = k * waveSpeed;
        frequency = wavelength * waveSpeed;
        timePerDiv = frequency;
        float p = 2 * Mathf.PI / k;
        lineRenderer.positionCount = lineSize;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            x += i * 0.001f;
            y = a * Mathf.Atan(Mathf.Tan(k * x - w * Time.time));
            lineRenderer.SetPosition(i, new Vector3(x, y, 0) + tempat);
        }
    }
    public void DrawNRamp(Vector3 positions, float amplitude, float wavelength, float waveSpeed, float frequency, float timePerDiv)
    {
        float x = 0f;
        float y;
        float a = amplitude / voltsPerDiv;
        float k = Mathf.PI * frequency * (timePerDiv * 10);
        float w = k * waveSpeed;
        frequency = wavelength * waveSpeed;
        timePerDiv = frequency;
        float p = 2 * Mathf.PI / k;
        lineRenderer.positionCount = lineSize;

        for (int i = 0; i < lineRenderer.positionCount; i++)
        {
            x += i * 0.001f;
            y = a * Mathf.Atan(Mathf.Tan(-k * x + w * Time.time));
            lineRenderer.SetPosition(i, new Vector3(x, y, 0) + tempat);
        }
    }
}
