using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCam : MonoBehaviour
{
    public GameObject camera;
    int dampenValue;
    bool startAgain;

    void Update()
    {
        
    }

    void follow()
    {
        StartCoroutine(DampenCoroutine());

        Vector3 rot = new Vector3(transform.localRotation.eulerAngles.x, camera.transform.localRotation.eulerAngles.y, transform.localRotation.eulerAngles.z);
        transform.localRotation = Quaternion.Euler(rot) ;
    }

    IEnumerator DampenCoroutine()
    {
        yield return new WaitForSecondsRealtime(dampenValue);
    }
}
