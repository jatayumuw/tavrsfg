using UnityEngine;
using UnityEngine.XR;

public class RotateUwU : MonoBehaviour
{
    float oldX;
    float oldY;

    public int rotatecount;
    public float minRotSpd = 0.4f;

    Vector2 inputAxis;

    void Update()
    {
        if (InputDevices.GetDeviceAtXRNode(XRNode.RightHand).TryGetFeatureValue(CommonUsages.primary2DAxis, out inputAxis))
        {
            float deltaValuex = inputAxis.x - oldX;
            float deltaValuey = inputAxis.y - oldY;
            oldX = inputAxis.x;
            oldY = inputAxis.y;

            if (deltaValuex >= minRotSpd || deltaValuey >= minRotSpd)
            {
                rotatecount += 1;
            }

            //Debug.Log(deltaValuex + ", " + deltaValuey);
        }
    }
}
