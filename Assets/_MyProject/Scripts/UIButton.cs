using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIButton : MonoBehaviour
{
    public GameObject BlackoutPanel;
    public int scene = 0;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    public void StartTutorial()
    {
        SceneManager.LoadScene("Tutorial");
        scene = 1;
    }
    public void StartSimulation()
    {
        SceneManager.LoadScene("Simulation");
        scene = 2;
    }
    public void StartTest()
    {
        SceneManager.LoadScene("TestCobaSequence1");
    }
    public void ExitGame()
    {
        Application.Quit(0);
    }
}

